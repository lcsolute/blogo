package dao

import (
	"awesomeProject/global"
	"awesomeProject/models"
	"fmt"
)

var user models.User

func FindUserInfo(username string, password string) (*models.User, bool){
	rows := global.DB.Where(&models.User{LoginUserName: username, LoginPassword:password}).Find(&user)
	fmt.Println(&user)
	if rows.RowsAffected < 1 {
		return &user, false
	}

	return &user, true
}

func FindUserById(userId int) (*models.User, bool){
	rows := global.DB.Where(&models.User{AdminUserId: userId}).Find(&user)
	if rows.RowsAffected < 1 {
		return &user, false
	}

	return &user, true
}