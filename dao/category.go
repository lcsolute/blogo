package dao

import (
	"awesomeProject/global"
	"awesomeProject/models"
)

var categories []models.Category

func GetCategoryAllList() (int, []interface{}){
	categoryList := make([]interface{}, 0, len(categories))
	result := global.DB.Find(&categories)
	if result.RowsAffected == 0 {
		return 0, categoryList
	}

	total := len(categories)

	for _, category := range categories {
		categoryItemMap := map[string]interface{}{
			"category_id": category.CategoryId,
			"category_name": category.CategoryName,
			"category_icon": category.CategoryIcon,
			"category_rank": category.CategoryRank,
			"is_deleted": category.IsDeleted,
			"create_time": category.CreateTime,
			"update_time": category.UpdateTime,
		}
		categoryList = append(categoryList, categoryItemMap)
	}
	return total, categoryList
}


