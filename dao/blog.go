package dao

import (
	"awesomeProject/global"
	"awesomeProject/models"
	"fmt"
	"gorm.io/gorm"
)

var blogs []models.Blog

func NewBlog(blog models.Blog) models.Blog {
	result := global.DB.Create(&blog)
	if result.RowsAffected == 0 {
		return models.Blog{}
	} else {

		return blog
	}
}

func UpdateUrlById(blog models.Blog) models.Blog {
	global.DB.Model(&blog).Update("blog_sub_url", blog.BlogSubUrl)
	return blog
}

func GetBlogListDao(page int, page_size int)  (int, []interface{}){

	blogList := make([]interface{}, 0, len(blogs))

	offset := (page - 1) * page_size

	result := global.DB.Offset(offset).Limit(page_size).Find(&blogs)

	if result.RowsAffected == 0 {
		return 0, blogList
	}

	total := len(blogs)


	blogList = toMapList(blogs, blogList)

	return total, blogList
}

func GetBlogTotal() int {
	var total int64
	result := global.DB.Table("tb_blog").Count(&total)
	if result.RowsAffected == 0 {
		return 0
	}
	return int(total)
}

func GetBlogById(blogId int) (blogMap map[string]interface{}) {
	var blog models.Blog
	result := global.DB.Where("blog_id = ?", blogId).Find(&blog)
	blogMap = toMap(blog)
	fmt.Println(blogMap)
	if result.RowsAffected == 0 {
		return
	}
	return
}

func QueryBlogListByKeyworks(keywords string, method int, offset int, limit int) (int, []interface{}){
	var blogList = make([]interface{}, 0, len(blogs))

	db := GetDB(global.DB, keywords, method)
	result := db.Offset(offset).Limit(limit).Find(&blogs)

	if result.RowsAffected == 0{
		return 0, blogList
	}

	blogList = toMapList(blogs, blogList)
	var total int64
	GetDB(global.DB, keywords, method).Table("tb_blog").Count(&total)
	// total == 0 : true
	fmt.Println("blogList:", total)
	return int(total), blogList
}



func toMapList(blogs []models.Blog, blogList []interface{}) []interface{}{
	for _, blog := range blogs {
		blogItemMap := toMap(blog)
		blogList = append(blogList, blogItemMap)
	}
	return blogList
}

func toMap(blog models.Blog) map[string]interface{}{
	user, _ := FindUserById(int(blog.BlogId))
	return map[string]interface{}{
		"blog_id":          blog.BlogId,
		"blog_title":       blog.BlogTitle,
		"blog_sub_url":     blog.BlogSubUrl,
		"blog_cover_image": blog.BlogCoverImage,
		"blog_content":     blog.BlogContent,
		"blog_category_id": blog.BlogCategoryId,
		"blog_tags":        blog.BlogTags,
		"blog_status":      blog.BlogStatus,
		"blog_views":       blog.BlogViews,
		"blog_likes": 		blog.BlogLikes,
		"author_name":  	user.NickName,
		"enable_comment":   blog.EnableComment,
		"is_deleted":       blog.IsDeleted,
		"create_time":      blog.CreateTime,
		"update_time":      blog.UpdateTime,
	}
}

func GetDB(db *gorm.DB, keywords string, method int) *gorm.DB{
	switch method {
	case 1: {// 根据 标题查询
		return db.Where("blog_title like ?", "%"+keywords+"%")
		// select * from tb_blog where blog_title like '%?%';
	}
	case 2:{// 根据 分类查询

		return db.Table("tb_blog").Where("blog_category_id in (?)", db.Table("tb_blog_category").Where("category_name like ?", "%"+keywords+"%").Select("category_id"))
		// select * from tb_blog where blog_category_id in (select category_id from tb_blog_category where category_name like '%G%');
	}
	case 3: { // 根据标签查询
		return db.Table("tb_blog").Where("blog_id in (?)", db.Table("tb_blog_tag_relation").Where("tag_id in (?)", db.Table("tb_blog_tag").Where("tag_name like (?)", "%"+keywords+"%").Select("tag_id")).Distinct("blog_id").Select("blog_id"))
		// select * from tb_blog where blog_id in (...)
	}
	default:
		return nil
	}
}

func AddLikesById(id int) int{
	var likes int = 0
	global.DB.Table("tb_blog").Where("blog_id = ?", id).Select("blog_likes").Find(&likes)

	likes += 1
	global.DB.Table("tb_blog").Where("blog_id = ?", id).Update("blog_likes", likes)
	return likes
}

func SubLikesById(id int) int {
	var likes int = 0
	global.DB.Table("tb_blog").Where("blog_id = ?", id).Select("blog_likes").Find(&likes)
	if likes <= 0 {
		return likes
	}
	likes -= 1
	global.DB.Table("tb_blog").Where("blog_id = ?", id).Update("blog_likes", likes)
	return likes
}

func AddViewsById(id int) int {
	var views int = 0
	global.DB.Table("tb_blog").Where("blog_id = ?", id).Select("blog_views").Find(&views)
	views += 1
	global.DB.Table("tb_blog").Where("blog_id = ?", id).Update("blog_views", views)
	return views

}

func DeleteBlogById(id int) int{

	result := global.DB.Delete(&models.Blog{}, id);
	return int(result.RowsAffected)
}
