package models

import "time"

type Blog struct {
	BlogId uint `json:"blog_id" gorm:"primaryKey"`
	BlogTitle string `json:"blog_title"`
	BlogSubUrl string `json:"blog_sub_url"`
	BlogCoverImage string `json:"blog_cover_image"`
	BlogContent string `json:"blog_content"`
	BlogCategoryId uint `json:"blog_category_id"`
	BlogTags string `json:"blog_tags"`
	BlogStatus uint `json:"blog_status"`
	BlogViews uint `json:"blog_views"`
	BlogLikes uint `json:"blog_likes"`
	AuthorID uint `json:"author_id"`
	EnableComment uint `json:"enable_comment"`
	IsDeleted uint `json:"is_deleted"`
	CreateTime *time.Time `json:"create_time"`
	UpdateTime *time.Time `json:"update_time"`

}

func (*Blog) TableName() string {
	return "tb_blog"
}
