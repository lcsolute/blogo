package models

type User struct {
	AdminUserId int `json:"admin_user_id" gorm:"primaryKey"`
	LoginUserName string `json:"login_user_name"`
	LoginPassword string `json:"login_password"`
	NickName string `json:"nick_name"`
	Locked int `json:"locked"`
}

func (*User) TableName() string{
	return "tb_admin_user"
}
