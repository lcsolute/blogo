package models

import "time"

type Category struct {
	CategoryId uint `json:"category_id" gorm:"primaryKey"`
	CategoryName string `json:"category_name"`
	CategoryIcon string `json:"category_icon"`
	CategoryRank int `json:"category_rank"`
	IsDeleted int `json:"is_deleted"`
	CreateTime *time.Time `json:"datetime"`
	UpdateTime *time.Time `json:"datetime"`
}

func (*Category) TableName() string{
	return "tb_blog_category"
}
