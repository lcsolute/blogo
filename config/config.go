package config

type ServerConfig struct {
	Name string `mapstructure:"name"`
	Port int `mapstructure:"port"`
	LogsAddress string `mapstr ucture:"logsAddress"`
	MysqlInfo MysqlConfig `mapstructure:"mysql"`
	RedisInfo  RedisConfig `mapstructure:"redis"`
	JWTKey JWTConfig `mapstructure:"jwt"`
}

type MysqlConfig struct {
	Host string `mapstructure:"host"`
	Port int `mapstructure:"port"`
	Password string `mapstructure:"password"`
	DBName string `mapstructure:"dbName"`
	Name string `mapstructure:"name"`
}

type RedisConfig struct {
	Host string `mapstructure:"host"`
	Port int `mapstructure:"port"`
}

type JWTConfig struct {
	SigningKey string `mapstructure:"key", json:"key"`
}