package controller

import (
	"awesomeProject/Response"
	"awesomeProject/dao"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func GetDashboardPage(c *gin.Context) {
	c.HTML(http.StatusOK, "manage.html", gin.H {
		"title": "Welcome to dashboard page",
	})
}

func GetDashboardInfo(c *gin.Context) {

	userIdStr, ok := c.Get("userId")
	if ok {
		userId, _ := strconv.Atoi(string(userIdStr.(uint)))
		user, _ := dao.FindUserById(userId)

		Response.Success(c, http.StatusOK, "success", user)
	}


}
