package controller

import (
	"awesomeProject/Response"
	"awesomeProject/dao"
	"github.com/gin-gonic/gin"
)

func GetCategoryAllList(c *gin.Context){
	total, categoryList := dao.GetCategoryAllList()
	if total + len(categoryList) == 0{
		Response.Err(c, 400, 400, "can not get data", map[string]interface{}{
			"total": total,
			"category_list":  categoryList,
		})
		return
	}

	Response.Success(c, 200, "get category list successfully", map[string]interface{}{
		"total": total,
		"category_list": categoryList,
	})
}
