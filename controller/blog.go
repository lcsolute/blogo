package controller

import (
	"awesomeProject/Response"
	"awesomeProject/dao"
	"awesomeProject/models"
	"awesomeProject/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"time"
)

func GetBlogPage(c *gin.Context) {
	c.HTML(http.StatusOK, "home.html", gin.H{
		"title": "blogo: a best blog website",
	})
}

func GetBlogList(c *gin.Context) {
	page, _ := strconv.Atoi(c.Param("page"))
	pageSize, _ := strconv.Atoi(c.Param("limit"))
	total, blogList := dao.GetBlogListDao(page, pageSize)

	if total + len(blogList) == 0 {
		Response.Err(c, 400, 400, "can not get data", map[string]interface{}{
			"total": total,
			"blog_list": blogList,
		})
		return
	}

	Response.Success(c, 200, "get blog list successfully", map[string]interface{}{
		"total": total,
		"blog_list": blogList,
	})
}

func GetBlogTotal(c *gin.Context) {
	tbTotal := dao.GetBlogTotal()
	if tbTotal == 0 {
		Response.Err(c, 400, 400, "The `tb_blog` is empty", map[string]interface{}{
			"tb_total": tbTotal,
		})
		return
	}

	Response.Success(c, 200, "get the total of blogs successfully", map[string]interface{}{
		"tb_total": tbTotal,
	})

}

func GetBlogDetail(c *gin.Context) {
	blogId, _ := strconv.Atoi(c.Query("id"))
	blog := dao.GetBlogById(blogId)
	Response.Success(c, 200, "success", map[string]interface{}{
		"blog":blog,
	})

}

func GetBlogDetailPage(c *gin.Context){
	c.HTML(200,"detail.html", gin.H{
		"title": "blog detail",
	})
}

// QueryBlogListByKeywords: 根据关键字查询博客列表
func QueryBlogListByKeywords(c *gin.Context) {
	input := c.Query("input")
	method, _ := strconv.Atoi(c.DefaultQuery("method", "1"))
	page, _ := strconv.Atoi(c.Param("page"))
	limit, _ := strconv.Atoi(c.Param("limit"))

	if method < 0 {
		Response.Err(c, 400, 400, "method must be a positive number", "")
		return
	}
	blogList := make([]interface{}, 0, 0)
	var total int

	offset := (page - 1) * limit
	total, blogList = service.QueryBlogListByKeyworks(input, method, offset, limit)
	if total == 0{
		Response.Err(c, 400, 400, "cannot find", "")
		return
	}
	Response.Success(c, 200, "success", map[string]interface{}{
		"blogs": blogList,
		"total": total,
	})

}

func AddLikes(c *gin.Context){
	UpdateLikes(c, dao.AddLikesById)
}

func SubLikes(c *gin.Context) {
	UpdateLikes(c, dao.SubLikesById)
}

func UpdateLikes(c *gin.Context, updateLikes func(id int) int) {
	id, _ := strconv.Atoi(c.Query("id"))
	likes := updateLikes(id)
	if likes == 0 {
		Response.Err(c, 400, 400, "fail to update likes", map[string]interface{}{
			"blog_likes": likes,
		})
		return
	}
	Response.Success(c, 200, "success", map[string]interface{}{
		"blog_likes": likes,
	})
}

func UpdateViews(c *gin.Context) {
	id, _ := strconv.Atoi(c.Query("id"))
	views := dao.AddViewsById(id)
	Response.Success(c, 200, "success", map[string]interface{}{
		"blog_views": views,
	})
}

func DeleteBlogById(c *gin.Context){
	id, _ := strconv.Atoi(c.Query("id"))
	if(id < 0) {
		Response.Err(c, 200, 400, "id 错误", "")
		return
	}

	var count int
	// 从数据库中删除
	count = dao.DeleteBlogById(id)
	if (count <= 0) {
		Response.Err(c, 200, 400, "删除失败", "")
		return
	}
	// 删除成功
	Response.Success(c, 200, "删除成功", map[string]interface{}{
		"count": count,
	})
	return

}

func GetEditPage(c *gin.Context) {
	c.HTML(200, "edit.html", gin.H{
		"title": "博客编辑页面",
	})
}

func PostImage(c *gin.Context) {
	file, _ := c.FormFile("file")
	dst := fmt.Sprintf("./static/images/%s", file.Filename)

	err := c.SaveUploadedFile(file, dst)
	if err != nil {
		Response.Err(c, 403, 403, "上传失败", "")
	}else {
		Response.Success(c, 200, "上传成功", map[string]interface{}{
			"url": fmt.Sprintf("/static/images/%s", file.Filename),
		})
	}
}

func NewBlog(c *gin.Context) {
	title := c.Query("title")
	content := c.Query("content")
	url := c.Query("url")
	createTime := time.Now()

	blog := models.Blog{
		BlogTitle:      title,
		BlogContent:    content,
		BlogCategoryId: 1,
		BlogTags:       "GO, Java",
		BlogCoverImage: url,
		BlogLikes:      0,
		BlogStatus:     1,
		BlogViews:      0,
		CreateTime:     &createTime,
	}

	blog = dao.NewBlog(blog)
	blog.BlogSubUrl = fmt.Sprintf("detail/page?id=%d", blog.BlogId)
	blog = dao.UpdateUrlById(blog)
	fmt.Println(blog)
	if blog.BlogId > 0{
		Response.Success(c, 200 , "success", map[string]interface{}{
			"blog": blog,
		})
	}

}