package controller

import (
  "awesomeProject/Response"
  "github.com/gin-gonic/gin"
  "github.com/mojocn/base64Captcha"
  "go.uber.org/zap"
  "net/http"
)
// Captcha 缓存对象
var store = base64Captcha.DefaultMemStore
// GetCaptcha 获取验证码
func GetCaptcha(c *gin.Context) {
  driver := base64Captcha.NewDriverDigit(80, 240, 5, 0.7, 80)

  cp := base64Captcha.NewCaptcha(driver, store)

  id, b64s, err := cp.Generate()
  if err != nil {
    zap.S().Errorf("generate captcha error:", err.Error())
    Response.Err(c, http.StatusInternalServerError, 500, "generate captcha error", "")
    return
  }

  Response.Success(c, http.StatusOK, "generate captcha successfully", gin.H{
    "captchaId": id,
    "picPath": b64s,
  })

}
