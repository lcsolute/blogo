package router

import (
	"awesomeProject/controller"
	"github.com/gin-gonic/gin"
)

func CategoryRouter(Router *gin.RouterGroup){
	CategoryRouter := Router.Group("/category")

	{
		CategoryRouter.GET("/all", controller.GetCategoryAllList)
	}
}
