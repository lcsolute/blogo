package router

import (
	"awesomeProject/controller"
	"github.com/gin-gonic/gin"
	"net/http"
)

func UserRouter(Router *gin.RouterGroup) {
	UserRouter := Router.Group("user")
	{
		UserRouter.GET("list", func(context *gin.Context) {
			context.JSON(200, gin.H{
				"message" : "pong",
			})
		})

		UserRouter.GET("/test", func(c *gin.Context) {
			c.HTML(http.StatusOK, "index.html", gin.H{
				"title": "Main website",
			})
		})

		UserRouter.POST("/login", controller.PasswordLogin)

	}
}