package router

import (
	"awesomeProject/controller"
	"awesomeProject/middlewares"
	"github.com/gin-gonic/gin"
)

func DashboardRouter(Router *gin.RouterGroup){
	DashboardRouter := Router.Group("/dashboard")
	{
		DashboardRouter.GET("/page",middlewares.JWTAuth(), controller.GetDashboardPage)
		DashboardRouter.GET("/content-manage", middlewares.JWTAuth(), controller.GetDashboardPage)
		DashboardRouter.GET("/info", middlewares.JWTAuth(), controller.GetDashboardInfo)
	}
}