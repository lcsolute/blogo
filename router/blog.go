package router

import (
	"awesomeProject/controller"
	"awesomeProject/middlewares"
	"github.com/gin-gonic/gin"
)

func BlogRouter(Router *gin.RouterGroup) {
	BlogRouter := Router.Group("/blog")

	{
		BlogRouter.GET("/page", controller.GetBlogPage)
		BlogRouter.GET("/list/:page/:limit", controller.GetBlogList)
		BlogRouter.GET("/total", controller.GetBlogTotal)
		BlogRouter.GET("/detail/get", controller.GetBlogDetail)
		BlogRouter.GET("/detail/page", controller.GetBlogDetailPage)
		BlogRouter.PUT("/update/likes/add", controller.AddLikes)
		BlogRouter.PUT("/update/likes/sub", controller.SubLikes)
		BlogRouter.PUT("/update/views/add", controller.UpdateViews)
		BlogRouter.DELETE("/delete", middlewares.JWTAuth(), controller.DeleteBlogById)
		BlogRouter.GET("/edit/page", middlewares.JWTAuth(), controller.GetEditPage)
		BlogRouter.POST("/post", middlewares.JWTAuth(), controller.PostImage)
		BlogRouter.POST("/new", middlewares.JWTAuth(), controller.NewBlog)

		BlogRouter.GET("/query/:page/:limit",controller.QueryBlogListByKeywords)

	}
}
