package router

import (
  "github.com/gin-gonic/gin"
  "awesomeProject/controller"
)

func LoginRouter(Router *gin.RouterGroup) {
  LoginRouter := Router.Group("/login")
  {
    LoginRouter.GET("/page", controller.GetLoginPage)
    LoginRouter.POST("/do", controller.DoLogin)
  }
}
