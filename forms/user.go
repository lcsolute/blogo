package forms

type PasswordLoginForm struct {
  Username string `form:"username" json:"username"`
  Password string `form:"password" json:"password" binding:"required,min=3,max=20"`
  Captcha string `form:"captcha" json:"captcha" bining:"min=5,max=5"`
  CaptchaId string `form:"captcha_id" json:"captcha_id" bining:"required"`
}
