package initialize

import (
	"awesomeProject/middlewares"
	"awesomeProject/router"
	"github.com/gin-gonic/gin"
)

func Routers() *gin.Engine{
	Router := gin.Default()

	ApiGroup := Router.Group("/blogo")
	router.UserRouter(ApiGroup)
	router.BlogRouter(ApiGroup)
	router.LoginRouter(ApiGroup)
	router.InitBaseRouter(ApiGroup)
	router.DashboardRouter(ApiGroup)
	router.CategoryRouter((ApiGroup))
	Router.Use(middlewares.GinLogger(), middlewares.GinRecovery(true))
	return Router
}
