package initialize

import (
	"awesomeProject/global"
	"awesomeProject/utils"
	"fmt"
	"go.uber.org/zap"
)

// InitLogger 初始化 Logger
func InitLogger() {
	// 实例化 zap 配置
	cfg := zap.NewDevelopmentConfig()
	// 配置日志输出的输出地址
	cfg.OutputPaths = []string{
		fmt.Sprintf("%slog_%s.log", global.Settings.LogsAddress, utils.GetNowFormatTodayTime()),
		"stdout",
	}

	// 创建 logger 实例
	logg, _ := cfg.Build()
	zap.ReplaceGlobals(logg) // 替换 zap 包中全局的 logger 实例，后续在其他包中只需要使用 zap.L() 调用即可
	global.Lg = logg
}
