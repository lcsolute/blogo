package initialize

import (
  "awesomeProject/global"
  "fmt"
  "github.com/go-redis/redis"
)

func InitRedis() {
  addr := fmt.Sprintf("%s:%d", global.Settings.RedisInfo.Host, global.Settings.RedisInfo.Port)

  global.Redis = redis.NewClient(&redis.Options{
    Addr: addr,
    Password: "",
    DB: 0,
  })

  _, err :=global.Redis.Ping().Result()
  if(err!=nil){
    panic(err)
  }
}
