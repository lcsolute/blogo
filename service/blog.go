package service

import (
	"awesomeProject/dao"
)

func QueryBlogListByKeyworks(keywords string, method int, offset int, limit int) (int, []interface{}){
	return dao.QueryBlogListByKeyworks(keywords, method, offset, limit);
}
