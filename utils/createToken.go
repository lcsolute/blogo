package utils

import (
	"awesomeProject/Response"
	"awesomeProject/middlewares"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"time"
)

func CreateToken(c *gin.Context, Id int, NickName string, Role int) string {
	// generate token information
	j := middlewares.NewJWT()
	claims := middlewares.CustomClaims {
		ID: uint(Id),
		NickName: NickName,
		AuthorityId: uint(Role),
		StandardClaims: jwt.StandardClaims{
			NotBefore: time.Now().Unix(),
			ExpiresAt: time.Now().Unix() + 60*60*24*30,
			Issuer: "test",
		},
	}
	token, err := j.CreateToken(claims)
	if err != nil {
		Response.Success(c, 401, "generate token failure, try again", "test")
	}
	return token
}
