function showRequest(formData, jqForm, options){
    // verify legal form field
    return true
}

function showResponse(responseText, statusText, xhr, $form) {
    // $(location).atrr('href', '/blogo/')
    localStorage.setItem("token", responseText.data.token);
    location.href = `/blogo/dashboard/page`;
}


var options = {
    beforeSubmit: showRequest,
    success: showResponse
};

$("form").ajaxForm(options);
