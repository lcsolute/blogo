// // exec js when document ready
// $(document).ready(function () {
//     var page, limit = 0; // page: the number of page, limit: the max of simple page
//     page = $("input.page").val(), limit = $("input.page-size").val(); // simulate to get 'page' and 'limit', page = $.(#page).get() with convert to integer and more.
//     page = page === undefined ? 1 : page;
//     limit = limit === undefined ? 5 : limit;
//     let total = 0; // 'total' for calculating the last page number
//     let dynamicPages, posts; // dynamicPages for displaying page number, and posts for displaying page list
//     // total = getPageTotal(); // get the page total from database
//     dynamicPages = generatePages(page, limit, total); // generate dynamic page number
//     getPosts(page, limit); // get post list about page and limit
//
//
// // // render page
// // // 上一页 - 首页- 。。。 - 下一页 - 尾页
// //     $("ul.pagination").append("<li class='page-item'><a class='page-link' href='/list/1/" + limit + "'>首页</a></li>");
// //     $("ul.pagination").append("<li class='page-item'><a class='page-link' href='/list/last/" + limit + "'>上一页</a></li>");
// //     for(var value in dynamicPages) {
// //         $("ul.pagination").append("<li class='page-item'><a class='page-link' href='/list/" + value + "/" + limit + "'>" + value + "</a></li>");
// //     }
// //     $("ul.pagination").append("<li class='page-item'><a class='page-link' href='/list/next/" + limit + "'>下一页</a></li>");
// //     $("ul.pagination").append("<li class='page-item'><a class='page-link' href='/list/" + Math.ceil(total / limit) + "/" + limit + "'>尾页</a></li>");
// //     $("ul.pagination").append(`<li class='page-item'>最多显示：<input class="page-size" value="5" type="number" style="width: 40px;"/></li>`)
// //
// // });
//
//
// function jumpPageAndLimit() {
//     let page = $("#page").val()
//     let pageSize = $("#page-size").val()
//
//     getPosts(page, pageSize)
// }
//
//
// // get post list about page and limit
//     function getPosts(page, limit) {
//         let url = "/blogo/blog/list/" + page + "/" + limit
//         $.get(url, function (res) {
//             if (res !== NaN && res.code === 200) {
//                 res.data.blog_list.forEach(function (blog) {
//                     $("div.post-list").append(`<div class="post-list-item">
//           <div class="post-title">
//             <a href="${blog.blog_sub_url}">${blog.blog_title}</a>
//           </div>
//           <div class="post-content">
//             <div class="post-summary text-ells">
//               ${blog.blog_content}
//
//             </div>
//             <a href="${blog.blog_sub_url}" class="post-readmore">阅读更多>></a>
//           </div>
//           <div class="post-desc">
//             posted @ ${blog.create_time} Chao
//             <span class="post-view-count">
//               阅读（${blog.blog_views}）
//             </span>
//             <span class="post-comment-count">
//               评论（2）
//             </span>
//             <span class="post-digg-count">
//               推荐（2）
//             </span>
//           </div>
//         </div>`)
//                 })
//                 $("#page").attr({value: page});
//                 $("#page-size").attr({value: page-size});
//             } else {
//                 alert("获取博文列表失败");
//                 return
//             }
//         })
//     }
//
// // generate dynamic page number
//     function generatePages(page, limit, total) {
//         let url = "/blogo/blog/total";
//         $.get(url, function (res) {
//             // console.log("total: " + res.data.tb_total);
//             // console.log(res.data !== NaN && res.code === 200);
//             if (res.data !== NaN && res.code === 200) {
//                 total = res.data.tb_total;
//                 // console.log("total:" + res.data.tb_total);
//                 let lastPage = Math.ceil(total / limit);
//                 if (lastPage <= 10) {
//                     let dynamicPages = Array(lastPage).fill().map((v, i) => i + 1);
//
//                     // render page
//                     // 上一页 - 首页- 。。。 - 下一页 - 尾页
//                     $("ul.pagination").append("<li class='page-item'><a class='page-link' href='/blogo/blog/list/1/" + limit + "'>首页</a></li>");
//                     $("ul.pagination").append(`<li class='page-item'><a class='page-link' href='/blogo/blog/list/${page-1}/${limit}>上一页</a></li>`);
//                     dynamicPages.forEach(function (value){
//                         $("ul.pagination").append("<li class='page-item'><a class='page-link' href='/blogo/blog/list/" + value + "/" + limit + "'>" + value + "</a></li>");
//                     })
//
//                     $("ul.pagination").append(`<li class='page-item'><a class='page-link' href='/blogo/blog/list/${page+1}/${limit}'>下一页</a></li>`);
//                     $("ul.pagination").append("<li class='page-item'><a class='page-link' href='/blogo/blog/list/" + Math.ceil(total / limit) + "/" + limit + "'>尾页</a></li>");
//                     $("ul.pagination").append(`<li class="page-item">第<input id="page" class="page" value="1" type="number" style="width: 40px;">页</li>`)
//                     $("ul.pagination").append(`<li class='page-item'>最多显示：<input id="page-size" class="page-size" value="5" type="number" style="width: 40px;"/>个</li>`)
//                     $("ul.pagination").append(`<li class="page-item"><input type="button" onClick="jumpPageAndLimit()" value="确定" width="40px"/></li>`)
//
//                 }
//
//                 // dynamicPages 为除第一页和最后一页之外的页码，-1 表示省略号
//                 let dynamicPages;
//
//                 if (page === 1) {
//                     dynamicPages = [2, 3, -1];
//                 } else if (page === 2) {
//                     dynamicPages = [2, 3, 4, -1];
//                 } else if (page === 3) {
//                     dynamicPages = [2, 3, 4, 5, -1];
//                 } else if (page === lastPage - 2) { // the last 3th
//                     dynamicPages = [-1, page - 2, page - 1, page, page + 1];
//                 } else if (page === lastPage - 1) { // the last 2th
//                     dynamicPages = [-1, page - 2, page - 1, page];
//                 } else {
//                     dynamicPages = [-1, page - 2, page - 1, page, page + 1, page + 2, -1];
//                 }
//
//                 dynamicPages.unshift(1);
//                 dynamicPages.push(lastPage);
//                 return dynamicPages;
//             } else {
//                 alert("数据库异常");
//                 return;
//             }
//         });
//     }
// });

// get the page total from database
// function getPageTotal() {
//     // send http request for getting total
//     let url = "/blogo/blog/total";
//     $.get(url, function(res) {
//         // console.log("total: " + res.data.tb_total);
//         if(res.data !== NaN && res.code === 200){
//             return res.data.tb_total;
//         } else {
//             alert("数据库异常");
//             return;
//         }
//     });
// }


$(window).load(()=>{
  // 根据 page, pageSize, total 生成页码
  function generatePages(page, pageSize, total) {
    let dynamicPages;
    let lastPage = Math.ceil(total / pageSize);
    if(lastPage <= 10) {
      return Array(lastPage).fill().map((v, i) => i + 1);
    }
    if (page === 1) {
        dynamicPages = [2, 3, -1];
    } else if (page === 2) {
        dynamicPages = [2, 3, 4, -1];
    } else if (page === 3) {
        dynamicPages = [2, 3, 4, 5, -1];
    } else if (page === lastPage - 2) { // the last 3th
        dynamicPages = [-1, page - 2, page - 1, page, page + 1];
    } else if (page === lastPage - 1) { // the last 2th
        dynamicPages = [-1, page - 2, page - 1, page];
    } else {
        dynamicPages = [-1, page - 2, page - 1, page, page + 1, page + 2, -1];
    }

    dynamicPages.unshift(1);
    dynamicPages.push(lastPage);
    return dynamicPages;
  }

  // 跳转页面，实际上是改变 input#page 的值，触发 input#page 的事件
  function jump(page, pageSize, total) {
    // 防止越界
    if(page <= 0) {
      page = 0;
    }
    if(page >= total){
      page = total;
    }
    // 更新 page
    $("input#page").attr({value: page});
  }

  // 局部刷新页面
  function flush(){
      let page = $("input#page").val();
      let pageSize = $("input#pageSize").val();

      // 页面没加载之前
      page = page === undefined ? 1 : page;
      pageSize = pageSize === undefined ? 5 : pageSize;

      // 获取 数据库中博客总数
      let total = 0;
      let url = `/blogo/blog/total`;
      $.get(url, function(res) {
        if(res !== null && res.code === 200){
          total = res.data.tb_total;

          // 生成页码
          let lastPage = Math.ceil(total / pageSize);
          let dynamicPages = generatePages(page, pageSize, total);
          // 渲染页码
          $("ul#pagination").empty();
          $("ul#pagination").append(`<li class="page-item"><a class="page-link" onclick="javascript:jump(1, ${pageSize}, ${total});">首页</a></li>`);
          $("ul#pagination").append(`<li class="page-item"><a class="page-link" onclick="javascript:jump(${page-1}, ${pageSize}, ${total});">上一页</a></li>`);
          dynamicPages.forEach((item, i) => {
            $("ul#pagination").append(`<li class="page-item"><a class="page-link" onclick="javascript:jump(${item}, ${pageSize}, ${total});">${item}</a></li>`);
          });
          $("ul#pagination").append(`<li class="page-item"><a class="page-link" onclick="javascript:jump(${page+1}, ${pageSize}, ${total});">下一页</a></li>`);
          $("ul#pagination").append(`<li class="page-item"><a class="page-link" onclick="javascript:jump(${lastPage}, ${pageSize}, ${total});">尾页</a></li>`);
          $("ul#pagination").append(`<li class="page-item">第<input id="page" class="page" value="1" type="number" style="width: 40px;">页</li>`)
          $("ul#pagination").append(`<li class='page-item'>最多显示：<input id="page-size" class="page-size" value="5" type="number" style="width: 40px;"/>个</li>`)
          $("ul#pagination").append(`<li class="page-item"><input id="jump-page-with-pageSize" type="button" value="确定" width="40px"/></li>`)

        } else {
          alert("数据库异常，页码生成失败，请刷新页面");
        }
      });

      // 获取博客列表
      url = `/blogo/blog/list/${page}/${pageSize}`;
      $.get(url, (res) => {
        if(res !== null && res.code === 200) {
          $("div.post_list").empty();
          res.data.blog_list.forEach( (blog) => {
              $("div.post-list").append(`<div class="post-list-item">
                                            <div class="post-title">
                                              <a href="${blog.blog_sub_url}">${blog.blog_title}</a>
                                            </div>
                                            <div class="post-content">
                                              <div class="post-summary text-ells">
                                                ${blog.blog_content}

                                              </div>
                                              <a href="${blog.blog_sub_url}" class="post-readmore">阅读更多>></a>
                                            </div>
                                            <div class="post-desc">
                                              posted @ ${blog.create_time} Chao
                                              <span class="post-view-count">
                                                阅读（${blog.blog_views}）
                                              </span>
                                              <span class="post-comment-count">
                                                评论（2）
                                              </span>
                                              <span class="post-digg-count">
                                                推荐（2）
                                              </span>
                                            </div>
                                          </div>`)
          });
        } else {
          alert("数据库异常，列表生成失败，请刷新页面");
        }
      });
    }

  // 刷新页面
  flush();
  // 绑定点击事件
  $("input#jump-page-with-pageSize").click(() => {
    alert("start binding...");
    flush();
  });

  // 给 page 绑定改变事件
  $("input#page").change(() => {
    flush();
  });
});
