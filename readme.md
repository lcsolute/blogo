## Blogo
Use `gin` to build a web application named "blogo".
It can manage your posts and record your ideas.

## Install
## Ubuntu Linux：
下载代码
```bash
$ mkdir awesomeProject
$ git init
$ git pull https://git.zituo.net/liangchao/blogo.git
```
安装 `docker``, 
```bash
$ sudo apt install docker-ce
$ sudo systemctl start docker
```
通过 `docker` 安装 `mysql`, `redis`


## Config
根据开发环境进行修改配置文件的`mysql`配置，`redis`配置，
默认为：
```yaml
# settings-dev.yaml
name: "go-gin"
port: 8080
logsAddress: "./logs"

mysql:
  name: "root"
  host: "192.168.16.130"
  port: 3306
  password: "123456"
  dbName: "my_blog_db"

redis:
  host: "192.168.16.130"
  port: 6379

jwt:
  key: "ufVpxUgnuN6ozHif"

```
## Launch
```bash
$ go build .
$ ./awesomeProject
```

前台： <a>localhost:8080/blogo/blog/page</a><br>
后台：  <a>localhost:8080/blogo/dashboard/page</a>

## Introduction to development

### Requirement

### Design

### Code
