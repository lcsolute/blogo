package main

import (
	"awesomeProject/global"
	"awesomeProject/initialize"
	"fmt"
	"go.uber.org/zap"
	"time"
)

func main() {
	initialize.InitConfig()

	Routers := initialize.Routers()

	Routers.LoadHTMLGlob("views/*")
	Routers.Static("/static", "./static")
	// 初始化日志信息
	initialize.InitLogger()
	// 初始化 mysql
	initialize.InitMysqlDB()
	// 初始化 redis
	initialize.InitRedis()
	// color.Yellow(">>>>>>>>>>>>> gin server started~~~")
	global.Redis.Set("test", "testValue", time.Second)
	// time.Sleep(time.Second * 2)
	value := global.Redis.Get("test")
	/// color.Blue(value.Val())
	fmt.Printf("test: ", value)
	// initial translation
	if err := initialize.InitTrans("zh"); err != nil {
		panic(err)
	}

	err := Routers.Run(fmt.Sprintf(":%d", global.Settings.Port))


	//
	if err != nil {
		zap.L().Info("this is hello func", zap.String("error", "启动错误"))
	}
}
